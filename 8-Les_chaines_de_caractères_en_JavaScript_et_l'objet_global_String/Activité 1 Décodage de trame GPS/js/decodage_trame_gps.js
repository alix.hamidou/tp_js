/*
Auteur : Hamidou Alix
Date : 04/05/2021
*/

//Objets
class Decodage_Trame_GPS{
    constructor(trame){                 //Constructeur
        this.trame = trame;                 //Definition de la trame
        this.tabChamps = trame.split(",");  //Decoupage des differant champ
        this.est_RMC = false;               //Si la trame est valide
        this.nbDegreLat = 0;                //Degrés en latitude
        this.nbMinLat = 0;                  //minute en latitude
        this.latNS = "";                    //Indicateur Nord Sud
        this.nbDegreLong = 0;               //Degrés en longitude
        this.nbMinLong = 0;                 //minute en longitude
        this.longEW = "";                   //Indicateur Est Ouest
        this.jour = 0;                      //Jour du mois
        this.mois = 0;                      //Mois de l'années
        this.annee = 0;                     //Années
        this.heure = 0;                     //Heure
        this.minute = 0;                    //Minute
        this.seconde = 0.0;                 //Seconde
        this.tabMois = ["Janv.","Fevr.","Mars","Avr.","Mai","Juin","Juil.","Août","Sept.","Oct.","Nov.","Dec."]
    }
    decoder(){                          //Decodage de la trame
        function verifierRMC(){             //Verification de la trame
            let result = true;                  //Resultat de l'analyse
            if(tabChamps.length != 12)result = false;    //Si la taille n'est pas bonne
            if(tabChamps[0] != "$GPRMC")result = false; //Si le type n'est pas bon
            if(tabChamps[1].length != 10)result = false; //Si la taille de l'heure n'est pas bon
            if(tabChamps[2] == "V")result = false;        //Si les données ne sont pas valide

            if(tabChamps[3].length != 9)result = false;  //Si la taille de la latitude n'est pas bon
            if(tabChamps[4] != "N" && tabChamps[4] != "S")result = false;//Si le N/S Indicator n'est pas Nord ou Sud

            if(tabChamps[5].length != 10)result = false; //Si la longitude n'est pas bon
            if(tabChamps[6] != "E" && tabChamps[6] != "W")result = false;//Si le E/W Indicator n'est pas Est ou Ouest
 
            if(tabChamps[9].length != 6)result = false;  //Si la date n'est pas valide
            
            return result;                              //On retourne le resultat de l'analyse
        }
        function extrairePosition(){        //Extraction de la position
            nbDegreLat = parseInt(tabChamps[3].substring(0,2));
            nbMinLat = parseFloat(tabChamps[3].substring(2,9));
            latNS = tabChamps[4];

            nbDegreLong = parseInt(tabChamps[5].substring(0,3));
            nbMinLong = parseFloat(tabChamps[5].substring(3,10));
            longEW = tabChamps[6];
        }
        function extraireDate(){            //Extraction de la date
            jour = parseInt(tabChamps[9].substring(0,2)); //Extraction du jour
            mois = parseInt(tabChamps[9].substring(2,4)); //Extraction du mois
            annee = parseInt(tabChamps[9].substring(4,6));//Extraction de l'annee

            mois = tabMois[mois-1];             //On prend le nom du mois dans le tableau

            if(annee>=50){                      //Si on est entre 50 et 99
                annee = 1900 + annee;               //On est en 1900
            }else{                              //Sinon
                annee = 2000 + annee;               //On est en 2000
            }
        }
        function extraireHeure(){           //Extraction de l'heure
            heure = parseInt(tabChamps[1].substring(0,2));  //Extraction de l'heure
            minute = parseInt(tabChamps[1].substring(2,4)); //Extraction des minutes
            seconde = parseFloat(tabChamps[1].substring(4,10));//Extraction des secondes
        }

        let tabChamps = this.tabChamps;     //Importation de la trame en local
        let tabMois = this.tabMois;         //Importation de la table des mois
        let nbDegreLat = 0;                 //Degrés en latitude
        let nbMinLat = 0;                   //minute en latitude
        let latNS = "";                     //Indicateur Nord Sud
        let nbDegreLong = 0;                //Degrés en longitude
        let nbMinLong = 0;                  //minute en longitude
        let longEW = "";                    //Indicateur Est Ouest
        let jour = 0;                       //Jour du mois
        let mois = 0;                       //Mois de l'années
        let annee = 0;                      //Années
        let heure = 0;                      //Heure
        let minute = 0;                     //Minute
        let seconde = 0.0;                  //Seconde

        this.est_RMC = verifierRMC();       //On verifie si la trame est complete
        if(this.est_RMC){                   //Si la trame est valide
            extrairePosition();                 //On extrait la position
            extraireDate();                     //On extrait la date
            extraireHeure();                    //On extrait l'heure
            this.nbDegreLat = nbDegreLat;       //Degrés en latitude
            this.nbMinLat = nbMinLat;           //minute en latitude
            this.latNS = latNS;                 //Indicateur Nord Sud
            this.nbDegreLong = nbDegreLong;     //Degrés en longitude
            this.nbMinLong = nbMinLong;         //minute en longitude
            this.longEW = longEW;               //Indicateur Est Ouest
            this.jour = jour;                   //Jour du mois
            this.mois = mois;                   //Mois de l'années
            this.annee = annee;                 //Années
            this.seconde = seconde;             //Seconde
            this.minute = minute;               //Minute
            this.heure = heure;                 //Heure
        }
    }
    lireLatitude(){                     //Retourne la latitude
        let msg;                            //Message final
        msg = this.nbDegreLat.toString()+"° "+this.nbMinLat.toString()+"' "+this.latNS;
        return msg;
    }
    lireLongitude(){                    //Retourne la longitude
        let msg = "";                       //Message final
        msg = this.nbDegreLong.toString()+"° "+this.nbMinLong.toString()+"' "+this.longEW;
        return msg;
    }
    lireDate(){                         //Retourne la date
        let msg = "";                       //Message final
        msg = this.jour.toString()+" "+this.mois+" "+this.annee.toString();
        return msg;
    }
    lireHeure(){                        //Retourne l'heure
        let msg = "";                       //Message final
        msg = this.heure.toString()+" H "+this.minute.toString()+" mn "+this.seconde.toString()+" s";
        return msg;
    }
    
}

//Variables
let trame_gps;                      //Données d'entrées
let objDecodage_Trame_GPS;          //Objet de decodage

//Programme
trame_gps = "$GPRMC,161229.487,A,3723.2475,N,12158.3416,W,0.13,309.62,120598,,*10\r\n";
objDecodage_Trame_GPS = new Decodage_Trame_GPS(trame_gps); //Creation de l'objet de décodage
objDecodage_Trame_GPS.decoder();

//Affichage
console.log("Informations contenues dans la trame :");
console.log("\tPosition :");
console.log("\t\tLatitude : "+objDecodage_Trame_GPS.lireLatitude());
console.log("\t\tLongitude : "+objDecodage_Trame_GPS.lireLongitude());
console.log("\tDate : ");
console.log("\t\t"+objDecodage_Trame_GPS.lireDate());
console.log("\Heure : ");
console.log("\t\t"+objDecodage_Trame_GPS.lireHeure());