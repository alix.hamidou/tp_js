## Version 1 Calcul d'IMC
![img1](V1-1.png)
![img2](V1-2.png)

## Version 2 Interpretation
![img3](V2.png)

## Version 3 Curseur
![img4](V3.png)

## Version 4 La balance
![img5](V4.png)

## Version 5 La silhouette
![img6](V5.png)

## Version 6 Homme ou Femme
![img7](V6.png)
