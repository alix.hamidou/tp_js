/*
Auteur : Hamidou Alix
Date : 18/05/2021
*/

function calculerIMC(prmPoids,prmTaille){   //Calcul de l'imc
    prmTaille = prmTaille/100;                  //On convertie de cm à m
    var imc = prmPoids/(prmTaille*prmTaille);   //Calcule de l'imc
    return imc;                                 //Retour de la valeur de l'imc
}

function interpretation(prmIMC){            //Retourne l'interprétation de l'imc
    //Variables
    var interpretation;                         //Interpretation de l'imc

    //Programme
    if(prmIMC<16.5){
        interpretation = "Dénutrition";
    }else if(prmIMC<18.5){
        interpretation = "Maigreur";
    }else if(prmIMC<25){
        interpretation = "Corpulence normale";
    }else if(prmIMC<30){
        interpretation = "Surpoids";
    }else if(prmIMC<35){
        interpretation = "Obésité modérée";
    }else if(prmIMC<40){
        interpretation = "Obésité sévère";
    }else{
        interpretation = "Obésité morbide";
    }
    return interpretation;
}

function calcul(){                          //Quand le bouton de calcul est presser
    //Variables
    var poids;                                  //Poids de l'utilisateur
    var taille;                                 //Taille de l'utilisateur
    var imc;                                    //IMC calculer

    var DOMPoids = document.getElementById("idSliderPoids");//Slider du poids
    var DOMTaille = document.getElementById("idSliderTaille");//Slider de la taille
    var DOMResult = document.getElementById("textIMC"); //Balise d'affichage

    //Programme
    poids = DOMPoids.value;                     //Récuperation du poids
    poids = poids.replace(",", ".");            //On remplace la virgule par un point pour la convertion en float
    poids = Number(poids);                      //On force le type

    taille = DOMTaille.value;                   //Récuperation de la taille
    taille = Number(taille);                    //On force le type
    if (isNaN(poids) || isNaN(taille)){         //Si la taille ou le poids n'est pas un nombre
        DOMResult.innerText = "Saisie incorrecte";  //Affichage de l'erreur
    }else{                                      //Sinon
        imc = calculerIMC(poids,taille);            //On calcul l'imc de l'utilisateur
        DOMResult.innerText = imc.toFixed(1)+"("+interpretation(imc)+")";//On affiche l'imc de l'utilisateur
    }

    afficherBalance(imc);                       //Affichage du graphique
    afficherSilhouette(imc);                    //Affichage de la silhouette
}

function poidsModifier(){           //Si on déplace le curseur poids
    //Variables
    var poids;                          //Poids actuel
    var DOMPoidsSpan = document.getElementById("textPoids");//Span d'affichage du poids
    var DOMSliderPoids = document.getElementById("idSliderPoids");//Slider du poids

    //Programme
    poids = DOMSliderPoids.value;       //On recupere le poids actuel
    DOMPoidsSpan.innerText = poids;     //On met a jour le poids afficher
    calcul();                           //On lance le calcul de l'imc
}
function tailleModifier(){          //Si on déplace le curseur de la taille
    //Variables
    var taille;                         //Taille actuel
    var DOMTailleSpan = document.getElementById("textTaille");//Span d'affichage de la taille
    var DOMSliderTaille = document.getElementById("idSliderTaille");//Slider de la taille

    //Programme
    taille = DOMSliderTaille.value;     //On recupere la taille actuel
    DOMTailleSpan.innerText = taille;   //On met a jour la taille afficher
    calcul();                           //On lance le calcul de l'imc
}

function afficherBalance(prmValImc){//Affichage du graphique
    //VARIABLES
    var deplacement;                    //Deplacement de l'aiguille en pixel

    var DOMAiguille = document.getElementById("aiguille");//Aiguille du graphique

    //PROGRAMME
    if(prmValImc>=10 && prmValImc<=45){ //Si l'imc entre dans la limite d'affichage
        deplacement = (300/35)*(prmValImc-10);   //Calcul de la position en pixel
        DOMAiguille.style.left = deplacement+"px";//On applique le deplacement de l'aiguille
    }

}

function afficherSilhouette(prmValImc){//Affichage de la silhouette
    //VARIABLES
    var decalage;                       //Variable du decallage en pixel
    var offset;                         //Offset des images

    DOMSilhouette = document.getElementById("silhouette");//Div de la silhouette

    //PROGRAMME
    offset = 105;                       //Offset des images
    if(prmValImc<16.5){
        decalage = 0;
    }else if(prmValImc<18.5){
        decalage = offset*(-1);
    }else if(prmValImc<25){
        decalage = offset*(-2);
    }else if(prmValImc<30){
        decalage = offset*(-3);
    }else if(prmValImc<35){
        decalage = offset*(-4);
    }else{
        decalage = offset*(-5);
    }

    DOMSilhouette.style.backgroundPosition = decalage+"px";//On applique le decallage

}

function changeSexe(){              //Si on change le sexe de la silhouette
    //VARIABLES
    var DOMSelectSexeHomme = document.getElementById("idHomme");//On recupere l'input radio de l'homme
    var DOMSilhouette = document.getElementById("silhouette");//Div de la silhouette

    //PROGRAMME
    if(DOMSelectSexeHomme.checked){ //Si l'homme est selectionné
        DOMSilhouette.style.backgroundImage = "url(css/img/IMC-homme.jpg)";
    }else{                          //Sinon c'est la femme
        DOMSilhouette.style.backgroundImage = "url(css/img/IMC-femme.jpg)";
    }

    calcul();                       //On lance le calcul de l'imc
}