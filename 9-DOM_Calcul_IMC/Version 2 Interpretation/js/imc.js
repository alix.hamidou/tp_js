/*
Auteur : Hamidou Alix
Date : 18/05/2021
*/

function calculerIMC(prmPoids,prmTaille){   //Calcul de l'imc
    prmTaille = prmTaille/100;                  //On convertie de cm à m
    var imc = prmPoids/(prmTaille*prmTaille);   //Calcule de l'imc
    return imc;                                 //Retour de la valeur de l'imc
}

function interpretation(prmIMC){            //Retourne l'interprétation de l'imc
    //Variables
    var interpretation;                         //Interpretation de l'imc

    //Programme
    if(prmIMC<16.5){
        interpretation = "Dénutrition";
    }else if(prmIMC<18.5){
        interpretation = "Maigreur";
    }else if(prmIMC<25){
        interpretation = "Corpulence normale";
    }else if(prmIMC<30){
        interpretation = "Surpoids";
    }else if(prmIMC<35){
        interpretation = "Obésité modérée";
    }else if(prmIMC<40){
        interpretation = "Obésité sévère";
    }else{
        interpretation = "Obésité morbide";
    }
    return interpretation;
}

function calcul(){                          //Quand le bouton de calcul est presser
    //Variables
    var poids;                                  //Poids de l'utilisateur
    var taille;                                 //Taille de l'utilisateur
    var imc;                                    //IMC calculer

    var DOMPoids = document.getElementById("idPoids");  //Balise du poids utilisateur
    var DOMTaille = document.getElementById("idTaille");//Balise de la taille utilisateur
    var DOMResult = document.getElementById("textIMC"); //Balise d'affichage

    //Programme
    poids = DOMPoids.value;                     //Récuperation du poids
    poids = poids.replace(",", ".");            //On remplace la virgule par un point pour la convertion en float
    poids = Number(poids);                      //On force le type

    taille = DOMTaille.value;                   //Récuperation de la taille
    taille = Number(taille);                    //On force le type
    if (isNaN(poids) || isNaN(taille)){         //Si la taille ou le poids n'est pas un nombre
        DOMResult.innerText = "Saisie incorrecte";  //Affichage de l'erreur
    }else{                                      //Sinon
        imc = calculerIMC(poids,taille);            //On calcul l'imc de l'utilisateur
        DOMResult.innerText = imc.toFixed(1)+"("+interpretation(imc)+")";//On affiche l'imc de l'utilisateur
    }
}