/*
Auteur : Hamidou Alix
Date : 26/04/2021
*/

//Variables
let listeArticles;      //Liste des articles [article1[Nom,PrixPour1,NbAcheté],article2[...],...] (X->3)
let article;            //Article actuellement selectionné
let total;              //Total de tout les articles

//Programme
listeArticles = [["Jus d'orange 1L",1.35,2],["Yaourt nature 4X",1.6,1],["Pain de mie 500g",0.9,1],["Barquette Jambon blanc 4xT",2.75,1],["Salade Laitue",0.8,1],["Spaguettis 500g",0.95,2]];
total = 0;
for(article of listeArticles){  //Pour chaque articles dans la liste
    console.log(article[0]);        //Affichage du nom
    console.log(article[2]+" X "+article[1]+"\t"+(article[1]*article[2])+" EUR");   //Affichage complémentaire du prix
    total += (article[1]*article[2]);//Ajout du prix dans le total
}

//Affichage
console.log("Nombre d'articles achetés : "+listeArticles.length);
console.log("MONTANT : "+total.toFixed(2)+" EUR");