/*
Auteur : Hamidou Alix
Date : 26/04/2021
*/

//Variables
let valeur;                                         //Compteur de boucle
let somme;                                          //Somme des valeurs
let listeValeurs;                                   //Liste de données

//Programme
somme = 0;
listeValeurs = [10,25,41,5,9,11,90,2,50,48]
console.log("Liste des valeurs : ");                //Affichage du message de base
for(valeur of listeValeurs){                        //Pour chaque valeur
    somme += valeur;                                    //On ajoute a la valeur total
    console.log(valeur);                                //On affiche la valeur
}
//Affichage
console.log("Somme des valeurs : "+somme);
console.log("Moyenne des valeurs : "+somme/listeValeurs.length);    //Affichage du resultat du calcul de la moyenne