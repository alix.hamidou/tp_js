/*
Auteur : Hamidou Alix
Date : 26/04/2021
*/

//Objet
function Patient(prmNom,prmPrenom,prmAge,prmSexe,prmTaille,prmPoids){
    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.age = prmAge;
    this.sexe = prmSexe;
    this.taille = prmTaille;
    this.poids = prmPoids;
    this.IMC = 0;
    this.interpretation = "";
    this.decrire = function(){
        let description;
        this.interpreter_IMC();
        if(this.sexe=="masculin")description = "Le patient "+this.nom+" "+this.prenom+" de sexe "+this.sexe+" est agé de "+this.age+" ans. Il mesure"+(this.taille/100).toFixed(0)+"m"+(this.taille%100)+" et pèse "+this.poids+"kg";
        if(this.sexe=="feminin")description = "La patiente "+this.nom+" "+this.prenom+" de sexe "+this.sexe+" est agé de "+this.age+" ans. Elle mesure"+(this.taille/100).toFixed(0)+"m"+(this.taille%100)+" et pèse "+this.poids+"kg";
        description += "\nSon IMC est de:"+this.IMC.toFixed(2);
        if(this.sexe=="masculin")description += "\nIl est en situation de "+this.interpretation;
        if(this.sexe=="feminin")description += "\nElle est en situation de "+this.interpretation;
        return description;
    }
    this.calculer_IMC = function(){
        this.IMC = this.poids/((this.taille/100)*(this.taille/100));
    }
    this.interpreter_IMC = function(){
        this.calculer_IMC();
        if(this.IMC<18.5)this.interpretation = "Sous-poids";
        if(this.IMC>=18.5 && this.IMC<25)this.interpretation = "Normal";
        if(this.IMC>=25 && this.IMC<30)this.interpretation = "Surpoids";
        if(this.IMC>=30 && this.IMC<35)this.interpretation = "Obese";
        if(this.IMC>=35)this.interpretation = "Obesite severe";
        return this.interpretation;
    }
}

//Fonction
function afficher_ListePatients(prmTabPatients){
    /*Creation d'une chaine de caractere des nom et prénom patients*/
    //Variables
    let msg;                    //Message préparé
    let patient;                //Patient actuelement selectionné

    //Programme
    msg = "";
    for(patient of prmTabPatients){ //Pour chaque patient
        msg += patient.prenom+" "+patient.nom+"\n";  //Prenom et Nom
    }
    return msg;                 //Renvoie du message préparé
}
function afficher_ListePatients_Par_Sexe(prmTabPatients,prmSexe){
    /*Affichage des patients par sexe*/
    //Variables
    let msg;                    //Message préparé
    let patient;                //Patient actuelement selectionné

    //Programme
    msg = "";
    for(patient of prmTabPatients){ //Pour chaque patient
        if(prmSexe==patient.sexe)msg+=patient.prenom+" "+patient.nom+"\n";//Si le sexe correspond, Prenom et Nom
    }
    return msg;                 //Renvoie du message préparé
}
function afficher_ListePatients_Par_Corpulence(prmTabPatients,prmCorpulence){
    /*Affichage des patients par corpulence*/
    //Variables
    let msg;                    //Message préparé
    let patient;                //Patient actuelement selectionné

    //Programme
    msg = "";
    for(patient of prmTabPatients){ //Pour chaque patient
        if(prmCorpulence==patient.interpreter_IMC())msg+=patient.prenom+" "+patient.nom+"avec un IMC = "+patient.IMC.toFixed(2)+"\n";//Si la corpullance correspond, Prenom, Nom et IMC
    }
    return msg;                 //Renvoie du message préparé
}
function afficher_DescriptionPatient(prmTabPatients,prmNom){
    /*Affichage de la description du patients*/
    //Variables
    let msg;                    //Message préparé
    let patient;                //Patient actuelement selectionné

    //Programme
    msg = "";
    for(patient of prmTabPatients){ //Pour chaque patient
        if(prmNom==patient.nom)msg = patient.decrire();//Si le nom correspond, Prenom, Nom et IMC
    }
    if(msg == "")msg="Ce nom n'existe pas dans la liste";//Si aucun patient touvé
    return msg;                 //Renvoie du message préparé
}

//Variables
let tabPatients;            //Liste des patients [ObjPatient1,ObjPatient2,...] (X)

//Programme
tabPatients = [
    new Patient("Dupond","Jean",30,"masculin",180,85),
    new Patient("Martin","Eric",42,"masculin",165,90),
    new Patient("Moulin","Isabelle",46,"féminin",158,74),
    new Patient("Verwaerde","Paul",55,"masculin",177,66),
    new Patient("Durand","Dominique",60,"féminin",163,54),
    new Patient("Lejeune","Bernard",63,"masculin",158,78),
    new Patient("Chevalier","Louise",35,"féminin",170,82)
];

//Affichage
// console.log("Liste des patients");
// console.log(afficher_ListePatients(tabPatients));

// console.log("Liste des patients de sexe féminin :");
// console.log(afficher_ListePatients_Par_Sexe(tabPatients,"féminin"));

// console.log("Liste des patients en état de surpoids :");
// console.log(afficher_ListePatients_Par_Corpulence(tabPatients,"Surpoids"));

console.log("Description du patient Dupond");
console.log(afficher_DescriptionPatient(tabPatients,"Dupond"));
console.log("Description du patient Brassart");
console.log(afficher_DescriptionPatient(tabPatients,"Brassart"));

