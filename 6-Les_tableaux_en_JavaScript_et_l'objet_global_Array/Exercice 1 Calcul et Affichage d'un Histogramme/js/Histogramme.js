/*
Auteur : Hamidou Alix
Date : 27/04/2021
*/

//Fonctions
function calculerRepartition(prmTabNotes){
    /*Retourne un tableau de la répartition des valeur*/
    //Variables
    let repartList;         //Liste de répartition [[TypeDeNote1,Effectif],[TypeDeNote2,Effectif],] (21->2)
    let note;               //Note actuel

    //Programme
    repartList = [[0,0],[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0],[12,0],[13,0],[14,0],[15,0],[16,0],[17,0],[18,0],[19,0],[20,0]];
    for(note of prmTabNotes){//Pour chaque note de la liste
        repartList[note][1]++;     //On augmente de 1 l'effectif de la note
    }
    return repartList;
}
function tracerHisto(prmTabRepart){
    /*Construction et affichage de l'histogramme*/
    //Variables
    let note;           //Repartition et note et la données actuel

    //Programme
    for(note of prmTabRepart){  //Pour chaque note
        console.log(note[0]+"\t"+("*").repeat(note[1]));    //Affichage de la note et sont effectif
    }
}

//Variables
let notes;              //Liste des notes [Note1,Note2,...] (X)
let repartition;        //Liste de répartition

//Programme
notes = [19,12,14,5,12,8,3,12,8,4,19,16,14,8,12,14,12,11,12,4,11,13];
repartition = calculerRepartition(notes);   //On calcul la répartition des notes

//Affichage
console.log(notes);
console.log(repartition);
tracerHisto(repartition);     //On trace l'histogramme