## Activité 1 Calcul de la moyenne d'une série de valeurs 
![img1](act1.png)

## Activité 2 Gestion d'une liste d'articles
![img2](act2.png)

## Activité 3 Utilisation d'un tableau pour gérer une liste de patients au niveau du calcul de l'IMC
![img3](act3.png)
![img4](act3-1.png)
![img5](act3-2.png)
![img6](act3-3.png)

## Exercice 1 Calcul et Affichage d'un Histogramme
![img7](exo1.png)
![img8](exo1-2.png)
