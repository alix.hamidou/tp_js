/*
Auteur : Hamidou Alix
Date : 07/04/2021
*/

//Variables
let taille;
let poids;
let IMC;
let interpretation;

//Programme
taille = 175;
poids = 100;
IMC = poids/((taille/100)*(taille/100));
if(IMC<16.5)interpretation = "Dénutrition";
if(IMC>=16.5 && IMC<18.5)interpretation = "Maigreur";
if(IMC>=18.5 && IMC<25)interpretation = "Corpulence normale";
if(IMC>=25 && IMC<30)interpretation = "Surpoids";
if(IMC>=30 && IMC<35)interpretation = "Obésité modérée";
if(IMC>=35 && IMC<40)interpretation = "Obésité sévère";
if(IMC>=40)interpretation = "Obésité morbide";

//Affichage
console.log("Calcul de l'IMC :");
console.log("Taille : "+taille+"cm");
console.log("Poids : "+poids+"kg");
console.log("IMC = "+IMC.toFixed(1));
console.log("Interprétation de l'IMC : "+interpretation);