/*
Auteur : Hamidou Alix
Date : 07/04/2021
*/

//Variables
let a;          //Valeur 1
let b;          //Valeur 2
let result;     //Resultat
let msg;        //Message préparé
let i;          //Boucle de multiplication
let j;          //Compteur de boucle

//Programme
a = 0;
b = 1;
i = 17;
msg = "Suite de Fibonacci : "+a+" "+b;
for(j=0;j<i;j++){//Pour chaque etape de la suite
    result = a+b;   //Calcul la suite
    a=b;            //On avance dans la suite
    b=result;
    msg += " "+result;//On ajoute le resultat au valeur final
}
console.log(msg);   //Affichage du message préparé