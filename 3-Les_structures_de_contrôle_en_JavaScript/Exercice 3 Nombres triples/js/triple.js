/*
Auteur : Hamidou Alix
Date : 07/04/2021
*/

//Variables
let n;          //nombre a multiplié par 3
let msg;        //Message préparé
let i;          //Boucle de multiplication
let j;          //Compteur de boucle

//Programme
n = 2;
msg = "Valeurs de la suite : "+n;
i = 12;
console.log("Valeur de départ : "+n);   //Affichage de départ
for(j=0;j<i;j++){                       //Pour chaque etape de multiplication
    n = n*3;                                //On triple la valeur
    msg += " "+n;                           //On ajoute le resultat au valeur final
}
console.log(msg);                       //Affichage de toute les valeurs