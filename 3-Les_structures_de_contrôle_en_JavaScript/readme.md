## Activité 1 Interprétation de l'IMC
![img1](act1.png)

## Activité 2 Conjecture de Syracuse
![img2](act2.png)
![img2](act2-1.png)

## Exercice 1 Calcul de factorielle
![img1](exo1.png)

## Exercice 2 Conversion EurosDollars
![img2](exo2.png)

## Exercice 3 Nombres triples
![img3](exo3.png)

## Exercice 4 Suite de Fibonacci
![img4](exo4.png)

## Exercice 5 Table de multiplication
![img5](exo5.png)
