/*
Auteur : Hamidou Alix
Date : 07/04/2021
*/

//Variables
let table;  //Valeur multiplié
let result; //Resultat
let msg;    //Message préparé
let i;      //Boucle de multiplication
let j;      //Compteur de boucle

//Programme
table = 7;
msg = "";
i = 20;

for(j=1;j<=i;j++){  //Pour chaque etape de la table
    result = table*j;   //Calcul de la valeur
    msg += " "+result;  //On ajoute au message préparé
    if(j%3 == 0){       //Si on a fait un packet de 3
        msg += " *"         //On ajoute au message une etoile
    }
}
console.log(msg);   //Affichage