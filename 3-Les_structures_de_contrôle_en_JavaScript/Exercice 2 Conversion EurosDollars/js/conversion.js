/*
Auteur : Hamidou Alix
Date : 07/04/2021
*/

//Variables
let dollarValue;        //Prix 1 euros en dollar
let euros;              //Boucle nombre d'euros

//Programme
dollarValue = 1.65;
euros = 1;
while(euros <= 16354){  //Tant qu'on arrive pas a 16384 euros
    console.log(euros+" euro(s) = "+(euros*dollarValue).toFixed(1)+" dollar(s)");//On affiche la conversion
    euros = euros*2;        //On passe au multiple suivant
}