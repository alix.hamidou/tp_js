/*
Auteur : Hamidou Alix
Date : 07/04/2021
*/

//Variables
let n;              //Nombre de facteur n
let i;              //Boucle d'affichage
let factor;         //Valeur max du vacteur
let result;         //Resultat
let msg;            //Message préparé

//Programme
factor = 10;
result = 0;
if(factor>1){       //Si le nombre est strictement superieur a 1
    console.log("1! = 1");//On affiche la premiere ligne
    for(n=2;n<=factor;n++){//Pour chaque valeur du facteur
        msg = n+"! = 1";    //On vide le message préparé et on remplace par celui de base
        result = 1;         //On reinitalise le resultat
        for(i=2;i<=n;i++){  //Pour chaque partie du calcul
            msg += " x "+i;     //On ajoute au message
            result = result*i;  //On calcul le resultat
        }
        msg += " = "+result;//On finie le message
        console.log(msg);   //Affichage
    }
}