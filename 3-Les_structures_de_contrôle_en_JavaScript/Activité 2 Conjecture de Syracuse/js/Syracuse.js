/*
Auteur : Hamidou Alix
Date : 07/04/2021
*/

//Variables
let NDeBase;
let N;
let tmpVol;
let altMax;
let resultMessage;

//Programme
NDeBase = 14;
N = NDeBase;
tmpVol = 0;
altMax = N;
resultMessage = N
while(N!=1){                    //Tant que N n'est pas egal a 1
    tmpVol++;                       //On ajoute 1 au temps de vol
    if(N%2==0){                     //Si N est pair
        N /= 2;                         //On le divise par deux
    }else{                          //Sinon
        N = (N*3)+1;                    //On multiplie par 3 et on ajoute 1
    }
    resultMessage += "-"+N;         //On ajoute le resultat au message de sortie
    if(N>altMax)altMax=N;           //Si N est plus haut que l'altitude max enregistré, on redéfinie le max trouvé
}
//Affichage
console.log("Suite de Syracuse pour "+NDeBase+":");
console.log(resultMessage);
console.log("Temps de vol = "+tmpVol);
console.log("Altitude maximale = "+altMax);