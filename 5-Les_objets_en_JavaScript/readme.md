## Activité 1 Création d'un objet littéral pour le calcul de l'IMC 
![img1](act1.png)

## Activité 2 Codage d'un constructeur d'objet pour le calcul de l'IMC
![img2](act2.png)
![img3](act2-1.png)

## Activité 3 Optimisation du codage du constructeur pour le calcul de l'IMC
![img4](act3.png)


## Activité 4 Implémentation de l'héritage pour gérer les professeurs et les élèves
![img5](act4.png)

## Exercice 1 Objet Jeux Video
![img6](exo1.png)