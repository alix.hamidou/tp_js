/*
Auteur : Hamidou Alix
Date : 26/04/2021
*/

//Objet
let objPatient = {
    nom:"Dupond",
    prenom:"Jean",
    age:30,
    sexe:"Masculin",
    taille:180,
    poids:85,
    definir_corpulence: function(){
        let nom = this.nom;
        let prenom = this.prenom;
        let age = this.age;
        let sexe = this.sexe;
        let taille = this.taille;
        let poids = this.poids;
        let IMC;
        let interpretation;
        function decrire(){
            let description;
            description = "LE patient "+nom+" "+prenom+" de sexe "+sexe+" est agé de "+age+" ans. Il mesure"+(taille/100).toFixed(0)+"m"+(taille%100)+" et pèse "+poids+"kg";
            return description;
        }
        function calculer_IMC(){
            IMC = poids/((taille/100)*(taille/100));
            return IMC;
        }
        function interpreter_IMC(){
            calculer_IMC();
            if(IMC<18.5)interpretation = "Sous-poids";
            if(IMC>=18.5 && IMC<25)interpretation = "Normal";
            if(IMC>=25 && IMC<30)interpretation = "Surpoids";
            if(IMC>=30 && IMC<35)interpretation = "Obese";
            if(IMC>=35)interpretation = "Obesite severe";
            return interpretation;
        }
        console.log(decrire());
        console.log("Son IMC est de:"+calculer_IMC().toFixed(2));
        console.log("Il est en situation de "+interpreter_IMC());
    }
}


//Programme
objPatient.definir_corpulence();
