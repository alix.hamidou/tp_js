/*
Auteur : Hamidou Alix
Date : 26/04/2021
*/

//Objet
function Patient(prmNom,prmPrenom,prmAge,prmSexe,prmTaille,prmPoids){
    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.age = prmAge;
    this.sexe = prmSexe;
    this.taille = prmTaille;
    this.poids = prmPoids;
    this.definir_corpulence = function(){
        let nom = this.nom;
        let prenom = this.prenom;
        let age = this.age;
        let sexe = this.sexe;
        let taille = this.taille;
        let poids = this.poids;
        let IMC;
        let interpretation;
        function decrire(){
            let description;
            if(sexe=="Masculin")description = "Le patient "+nom+" "+prenom+" de sexe "+sexe+" est agé de "+age+" ans. Il mesure"+(taille/100).toFixed(0)+"m"+(taille%100)+" et pèse "+poids+"kg";
            if(sexe=="Feminin")description = "La patiente "+nom+" "+prenom+" de sexe "+sexe+" est agé de "+age+" ans. Elle mesure"+(taille/100).toFixed(0)+"m"+(taille%100)+" et pèse "+poids+"kg";
            return description;
        }
        function calculer_IMC(){
            IMC = poids/((taille/100)*(taille/100));
            return IMC;
        }
        function interpreter_IMC(){
            calculer_IMC();
            if(IMC<18.5)interpretation = "Sous-poids";
            if(IMC>=18.5 && IMC<25)interpretation = "Normal";
            if(IMC>=25 && IMC<30)interpretation = "Surpoids";
            if(IMC>=30 && IMC<35)interpretation = "Obese";
            if(IMC>=35)interpretation = "Obesite severe";
            return interpretation;
        }
        console.log(decrire());
        console.log("Son IMC est de:"+calculer_IMC().toFixed(2));
        if(sexe=="Masculin")console.log("Il est en situation de "+interpreter_IMC());
        if(sexe=="Feminin")console.log("Elle est en situation de "+interpreter_IMC());
    }
}


//Programme
let patient1 = new Patient("Dupond","Jean",30,"Masculin",180,85);
let patient2 = new Patient("Moulin","Isabelle",46,"Feminin",158,74);
let patient3 = new Patient("Martin","Eric",42,"Masculin",165,90);
patient1.definir_corpulence();
patient2.definir_corpulence();
patient3.definir_corpulence();
