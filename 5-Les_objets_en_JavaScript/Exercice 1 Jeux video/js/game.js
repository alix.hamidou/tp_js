/*
Auteur : Hamidou Alix
Date : 07/04/2021
*/

//Objet
//Constructeur
function Personnage(prmNom,prmNiveau){
    //Nom
    this.nom = prmNom;
    //Niveau
    this.niveau = prmNiveau;
    //Arme
    this.arme = "";
}
//Prototype de Personnage Saluer
Personnage.prototype.saluer = function(){
    return this.nom+" vous salue!!!";
}

//Constructeur simplifier
function Gerrier(prmNom,prmNiveau,prmArme){
    //Heritage
    Personnage.call(this,prmNom,prmNiveau);
    //Arme
    this.arme = prmArme;
}
//On ratache les prototype de Personnage a Gerrier (METTRE AVANT)
Gerrier.prototype = Object.create(Personnage.prototype);
//Prototype de Gerrier Combattre
Gerrier.prototype.Combattre = function(){
    return this.nom+" est un guerrier qui se bat avec "+this.arme;
}


function Magicien(prmNom,prmNiveau,prmPouvoir){
    //Heritage
    Personnage.call(this,prmNom,prmNiveau);
    //Arme
    this.pouvoir = prmPouvoir;
}
//On ratache les prototype de Personnage a Magicien (METTRE AVANT)
Magicien.prototype = Object.create(Personnage.prototype);
//Prototype de Magicien Combattre
Magicien.prototype.posseder = function(){
    return this.nom+" est un magicien qui possède le pouvoir de "+this.pouvoir;
}




//Programme
//Creation du personnage
let objArthur = new Gerrier("Arthur",3,"épé");
console.log(objArthur.saluer());
console.log(objArthur.Combattre());
let objMerlin = new Magicien("Merlin",2,"prédire les batailles");
console.log(objMerlin.saluer());
console.log(objMerlin.posseder());
