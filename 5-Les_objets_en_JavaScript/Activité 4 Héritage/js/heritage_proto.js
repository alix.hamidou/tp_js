/*
Auteur : Hamidou Alix
Date : 26/04/2021
*/

//Objet
//Constructeur Personne
function Personne(prmNom,prmPrenom,prmAge,prmSexe){
    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.age = prmAge;
    this.sexe = prmSexe;
}
//Personne a pour prototype Decrire
Personne.prototype.decrire = function(){
    let description;
    description = "Cette personne s'appelle " + this.prenom + " " + this.nom + " elle est agée de " + this.age + " ans" ;
    return description;
}

//Creation de professeur, il utilise le constructeur Personne
function Professeur(prmNom,prmPrenom,prmAge,prmSexe,prmMatiere){
    Personne.call(this,prmNom, prmPrenom, prmAge, prmSexe);
    this.matiere = prmMatiere;
}
//On ratache les prototype de personne a Professeur (METTRE AVANT LES AUTRE PROTOTYPE)
Professeur.prototype = Object.create(Personne.prototype);
//Professeur a pour prototype decrire_plus
Professeur.prototype.decrire_plus = function() {
    let description;
    let prefixe;
    if(this.sexe == 'M') {
        prefixe = 'Mr';
    } else {
        prefixe = 'Mme';
    }
    description = prefixe + " " + this.prenom + " " + this.nom + " est professeur de " + this.matiere;
    return description;
}

//Creation de Eleve, il utilise le constructeur Personne
function Eleve(prmNom,prmPrenom,prmAge,prmSexe,prmClasse){
    Personne.call(this,prmNom, prmPrenom, prmAge, prmSexe);
    this.classe = prmClasse;
}
//On ratache les prototype de personne a Eleve (METTRE AVANT LES AUTRE PROTOTYPE)
Eleve.prototype = Object.create(Personne.prototype);
//Eleve a pour prototype decrire_plus
Eleve.prototype.decrire_plus = function() {
    let description;
    description = this.prenom + " " + this.nom + " est un élève de " + this.classe;
    return description;
}

//Programme
let objProfesseur1 = new Professeur('Dupond', 'Jean', 30, 'M', 'Mathématiques');
let objEleve = new Eleve('Dutillieul', 'Dorian', 18, 'M', 'SNIR1');
console.log(objProfesseur1.decrire());
console.log(objProfesseur1.decrire_plus());
console.log(objEleve.decrire());
console.log(objEleve.decrire_plus());
