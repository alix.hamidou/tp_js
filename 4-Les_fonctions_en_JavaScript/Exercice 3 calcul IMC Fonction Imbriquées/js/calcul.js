/*
Auteur : Hamidou Alix
Date : 26/04/2021
*/
let taille;
let poids;
taille = 160;
poids = 100;

function decrire_corpulence(prmTaille,prmPoids){
    let IMC;
    let interpretation;

    function calculerIMC(){
        IMC = prmPoids/((prmTaille/100)*(prmTaille/100));
    }
    function interpreterIMC(){
        if(IMC<18.5)interpretation = "Sous-poids";
        if(IMC>=18.5 && IMC<25)interpretation = "Normal";
        if(IMC>=25 && IMC<30)interpretation = "Surpoids";
        if(IMC>=30 && IMC<35)interpretation = "Obese";
        if(IMC>=35)interpretation = "Obesite severe";
    }

    calculerIMC();
    interpreterIMC();
    console.log("Votre IMC est égal à "+IMC.toFixed(1)+" : Vous êtes en état de "+interpretation);
}

//Affichage
console.log("Calcul de l'IMC :");
console.log("Taille : "+taille+"cm");
console.log("Poids : "+poids+"kg");
decrire_corpulence(taille,poids);