## Exercice 1 calcul du temps de parcours d'un trajet

![img1](exo1.png)
![img1](exo1-1.png)

## Exercice 2 recherche du nombre de multiples de 3

![img2](exo2.png)

## Exercice 3 calcul IMC Fonction Imbriquées

![img3](exo3.png)
