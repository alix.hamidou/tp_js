/*
Auteur : Hamidou Alix
Date : 07/04/2021
*/

//Variables
//Vitesse
let vitesse = 90;
//Distance
let distance = 500;
//Resultat en temps
let tempsS;
let tempsM;
let tempsH;

//Fonction
function calculerTempsParcoursSec(prmVitesse,prmDistance){
    return (prmDistance/prmVitesse)*3600;
}

//Programme
//Appele de la fonction
tempsS = calculerTempsParcoursSec(vitesse,distance);
tempsM = Math.floor(tempsS/60);
tempsS -= tempsM*60;
tempsH = Math.floor(tempsM/60);
tempsM -= tempsH*60;
//Affichage
console.log("Vitesse moyenne (en km/h)  "+vitesse);
console.log("Distance à parcourir (en km)   "+distance);
// console.log("A "+vitesse+" km/h, une distance de "+distance+" km est parcourue en "+temps+" s");
console.log("A "+vitesse+" km/h, une distance de "+distance+" km est parcourue en "+tempsH+"H "+tempsM+"mm "+tempsS+"s");