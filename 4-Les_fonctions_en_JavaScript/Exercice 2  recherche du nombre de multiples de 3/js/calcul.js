/*
Auteur : Hamidou Alix
Date : 07/04/2021
*/

//Variables
//Nombre de multiplication
let nbMul = 20;

//Fonction
function rechercher_Mult3(prmNbMultiple){
    //Message préparé
    let msg = "Multiples de 3 : 0";
    //Resultat
    let result = 0;
    //Boucle de multiplication
    for(let i=1;i<prmNbMultiple;i++){
        //Calcul de multiplication
        result = 3 * i;
        //Ajout au message
        msg += "-"+result;
    }
    //ON retourne le message préparé
    return msg;
}

//Programme
//Affichage
console.log(rechercher_Mult3(nbMul));