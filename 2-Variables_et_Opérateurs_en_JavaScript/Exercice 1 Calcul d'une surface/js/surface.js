/*
Auteur : Hamidou Alix
Date : 07/04/2021
*/
//Variables
let longueur;
let largeur;
let surface;

//Programme
longueur = 2;
largeur = 3;
surface = longueur*largeur;

//Affichage
console.log("Longueur de la piece: "+longueur+"m");
console.log("Largeur de la piece: "+largeur+"m");
console.log("Surface de la piece: "+surface+"m²");