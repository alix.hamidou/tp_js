/*
Auteur : Hamidou Alix
Date : 07/04/2021
*/

//Variables
let degre;
let fahrenheit;

//Programme
degre = 22.6;
fahrenheit = degre*1.8+32;

//Affichage
console.log("Conversion Celcius (°C) / Fahrenheit (°F)");
console.log("une température de "+degre+"°C correspond à une température de "+fahrenheit.toFixed(1)+"°F");