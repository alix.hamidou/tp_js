/*
Auteur : Hamidou Alix
Date : 07/04/2021
*/
//Variables
let taille;
let poids;
let imc;

//Programme
taille = 160;
poids = 100;
imc = poids/((taille/100)*(taille/100));

//Affichage
console.log("Calcul de l'IMC :");
console.log("Taille : "+taille+"cm");
console.log("Poids : "+poids+"kg");
console.log("IMC = "+imc.toFixed(1));