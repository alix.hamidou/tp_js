/*
Auteur : Hamidou Alix
Date : 28/04/2021
*/

//Classe
//Classe personnage
class Personnage{
    //Constructeur de la classe Personnage
    constructor(prmNom,prmNiveau){
        this.nom = prmNom;
        this.niveau = prmNiveau;
        this.arme = "";
    }
    //Retourne le message de salutation
    Saluer(){
        return this.nom+" vous salue!!!";
    }
}
//Classe Gerrier qui hérite de la classe Personnage
class Gerrier extends Personnage{
    //Constructeur de la classe Gerrier
    constructor(prmNom,prmNiveau,prmArme){
        //On lance le constructeur de la classe Personnage
        super(prmNom,prmNiveau);
        this.arme = prmArme;
    }
    //Retourne le message de combat du gerrier
    Combattre(){
        return this.nom+" est un guerrier qui se bat avec "+this.arme;
    }
}
//Classe Magicien qui hérite de la classe Personnage
class Magicien extends Personnage{
    //Constructeur de la classe Magicien
    constructor(prmNom,prmNiveau,prmPouvoir){
        //On lance le constructeur de la classe Personnage
        super(prmNom,prmNiveau);
        this.pouvoir = prmPouvoir;
    }
    //Retourne le message de combat du Magicien
    Posseder(){
        return this.nom+" est un magicien qui possède le pouvoir de "+this.pouvoir;
    }
}

//Variables

//Programme
//Creation des objets
let objArthur = new Gerrier("Arthur",3,"épé");
let objMerlin = new Magicien("Merlin",2,"prédire les batailles");

//Affichage
//Affichage du Gerrier
console.log(objArthur.Saluer());
console.log(objArthur.Combattre());
//Affichage du Magicien
console.log(objMerlin.Saluer());
console.log(objMerlin.Posseder());
