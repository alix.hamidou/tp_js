/*
Auteur : Hamidou Alix
Date : 28/04/2021
*/

//Classe
class Patient{
    //Constructeur
    constructor(prmNom,prmPrenom,prmAge,prmSexe,prmTaille,prmPoids){
        this.nom = prmNom;
        this.prenom = prmPrenom;
        this.age = prmAge;
        this.sexe = prmSexe;
        this.taille = prmTaille;
        this.poids = prmPoids;
    }
    //Retourne le message d'identification du patient
    definir_corpulance(){
        let nom = this.nom;
        let prenom = this.prenom;
        let age = this.age;
        let sexe = this.sexe;
        let taille = this.taille;
        let poids = this.poids;
        let IMC;
        let interpretation;
        let finalMessage = "";              //Message de retour de la description
        function decrire(){
            let description;
            //Si le patient est un homme ou femme
            if(sexe=="Masculin")description = "Le patient "+nom+" "+prenom+" de sexe "+sexe+" est agé de "+age+" ans. Il mesure"+parseInt(taille/100)+"m"+(taille%100)+" et pèse "+poids+"kg";
            if(sexe=="Feminin")description = "La patiente "+nom+" "+prenom+" de sexe "+sexe+" est agé de "+age+" ans. Elle mesure"+parseInt(taille/100)+"m"+(taille%100)+" et pèse "+poids+"kg";
            return description;
        }
        function calculer_IMC(){
            //Calcul de l'imc
            IMC = poids/((taille/100)*(taille/100));
            return IMC;
        }
        function interpreter_IMC(){
            //Demande de calcul de l'imc
            calculer_IMC();
            //Pour chaque type de corpulance
            if(IMC<18.5)interpretation = "Sous-poids";
            if(IMC>=18.5 && IMC<25)interpretation = "Normal";
            if(IMC>=25 && IMC<30)interpretation = "Surpoids";
            if(IMC>=30 && IMC<35)interpretation = "Obese";
            if(IMC>=35)interpretation = "Obesite severe";
            return interpretation;
        }
        //On ajoute la description du patient dans le message finale
        finalMessage += decrire();
        //On ajoute l'interprétation du patient selon si il est un homme ou une femme
        finalMessage += "\nSon IMC est de:"+calculer_IMC().toFixed(2);
        if(sexe=="Masculin")finalMessage += "\nIl est en situation de "+interpreter_IMC();
        if(sexe=="Feminin")finalMessage += "\nElle est en situation de "+interpreter_IMC();
        return finalMessage;
    }
}

//Variables

//Programme
//Creation des patients
let patient1 = new Patient("Dupond","Jean",30,"Masculin",180,85);
let patient2 = new Patient("Moulin","Isabelle",46,"Feminin",158,74);
let patient3 = new Patient("Martin","Eric",42,"Masculin",165,90);

//Affichage
//Affichage des descriptions de chaque patients
console.log(patient1.definir_corpulance());
console.log(patient2.definir_corpulance());
console.log(patient3.definir_corpulance());