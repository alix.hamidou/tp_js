/*
Auteur : Hamidou Alix
Date : 28/04/2021
*/

//Classe
class Personne{
    //Constructeur de la classe personne
    constructor(prmNom,prmPrenom,prmAge,prmSexe){   
        this.nom = prmNom;
        this.prenom = prmPrenom;
        this.age = prmAge;
        this.sexe = prmSexe;
    }
    //Retourne la description de la personne
    decrire(){
        let description;
        description = "Cette personne s'appelle " + this.prenom + " " + this.nom + " elle est agée de " + this.age + " ans" ;
        return description;
    }
}
//Classe Professeur Héritant de la classe Personne
class Professeur extends Personne{
    //Constucteur de la classe Professeur
    constructor(prmNom,prmPrenom,prmAge,prmSexe,prmMatiere){
        //On lance la construction de la classe Personne
        super(prmNom,prmPrenom,prmAge,prmSexe);
        this.matiere = prmMatiere;
    }
    //Retourne la description du professeur
    decrire_plus(){
        let description;
        let prefixe;
        if(this.sexe == 'M') {
            prefixe = 'Mr';
        } else {
            prefixe = 'Mme';
        }
        description = prefixe + " " + this.prenom + " " + this.nom + " est professeur de " + this.matiere;
        return description;
    }
}
//Classe Eleve Héritant de la classe Personne
class Eleve extends Personne{
    //Constucteur de la classe Eleve
    constructor(prmNom,prmPrenom,prmAge,prmSexe,prmClasse){
        //On lance la construction de la classe Personne
        super(prmNom,prmPrenom,prmAge,prmSexe);
        this.classe = prmClasse;
    }
    //Retourne la description de l'eleve
    decrire_plus(){
        let description;
        description = this.prenom + " " + this.nom + " est un élève de " + this.classe;
        return description;
    }
}


//Variables

//Programme
//Creation du professeur
let objProfesseur1 = new Professeur('Dupond', 'Jean', 30, 'M', 'Mathématiques');
//Creation de l'eleve
let objEleve = new Eleve('Dutillieul', 'Dorian', 18, 'M', 'SNIR1');

//Affichage
//Description du professeur
console.log(objProfesseur1.decrire());
//Description supplémentaire du professeur
console.log(objProfesseur1.decrire_plus());
//Description de l'eleve
console.log(objEleve.decrire());
//Description supplémentaire de l'eleve
console.log(objEleve.decrire_plus());